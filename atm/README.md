# Joshua Chanin

## ATM Project

### Process
1. Install NetBeans
2. Created ATM interface
3. Coded pin system that allows user access; buttons highlight and become clickable when correct pin is entered
4. Loan feature is fully functional
5. Able to produce receipt for loan
6. Able to check balance, deposit, and withdraw

### Future Goals and Fixes
1. Fix loop if more than one transaction is needed
2. Receipt for withdraw, loan, and balance
3. Fix Enter button so that it works when entering withdraw and deposit numbers
4. Integrate database that contains multiple banking accounts
5. Back button to return to main menu
6. Prompt page asking for desired transaction
7. Ability to actually print receipt


![ATM-StartUp](img/startup.png)

Application Startup

![ATM-Pin](img/pin.png)

Enter User Pin

![ATM-Invalid](img/invalid.png)

Wrong Pin Means No Access

![ATM-Options](img/options.png)

Correct Pin Allows Access to System

![ATM-Loan](img/loan.png)

Able to Request a Loan

![ATM-Receipt](img/receipt.png)

Generate a Receipt for a Loan